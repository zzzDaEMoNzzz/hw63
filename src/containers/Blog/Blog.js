import React, {Component} from 'react';
import {withRouter, Route, Switch} from "react-router-dom";
import axios from "axios";

import './Blog.css';
import Header from "../../components/Header/Header";
import Posts from "../Posts/Posts";
import About from "../../components/About/About";
import Contacts from "../../components/Contacts/Contacts";


class Blog extends Component {
  state = {
    posts: [],
    preloaderEnabled: false,
  };

  preloaderShow = () => {
    if (!this.state.preloaderEnabled)
    this.setState({preloaderEnabled: true});
  };

  preloaderHide = () => {
    if (this.state.preloaderEnabled)
    this.setState({preloaderEnabled: false});
  };

  getPosts = () => {
    this.preloaderShow();

    axios.get('posts.json').then(response => {
      if (response.hasOwnProperty('data')) {
        if (response.data) {
          const postsID = Object.keys(response.data);
          const posts = postsID.map(id => {
            return {
              id,
              ...response.data[id]
            };
          });
          this.setState({posts});
        } else this.setState({posts: []});
      }
    }).finally(() => this.preloaderHide());
  };

  componentDidMount() {
    this.getPosts();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname === '/' && (this.props.location.pathname !== prevProps.location.pathname)) {
      this.getPosts();
    }
  }

  render() {
    const isPostsPage = (match, location) => {
      return (location.pathname === '/' || location.pathname === '/posts' || location.pathname === '/posts/');
    };

    return (
      <div className="Blog">
        <div className="Preloader" hidden={!this.state.preloaderEnabled} />
        <Header
          logo="My Blog"
          links={[
            {text: 'Home', url: '/', exact: true, isActive: isPostsPage},
            {text: 'Add', url: '/posts/add'},
            {text: 'About', url: '/about'},
            {text: 'Contacts', url: '/contacts'},
          ]}
        />
        <div className="Blog-body">
          <Switch>
            <Route path="/" exact render={() => (
              <Posts
                posts={this.state.posts}
                preloaderShow={this.preloaderShow}
                preloaderHide={this.preloaderHide}
              />
            )}/>
            <Route path="/posts/" render={() => (
              <Posts
                posts={this.state.posts}
                preloaderShow={this.preloaderShow}
                preloaderHide={this.preloaderHide}
              />
            )}/>
            <Route path="/about/" component={About}/>
            <Route path="/contacts/" component={Contacts}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(Blog);