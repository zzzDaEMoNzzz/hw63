import React from 'react';
import './PostAdd.css';

const PostAdd = ({title, description, onTitleChange, onDescriptionChange, addPost}) => {
  return (
    <form className="PostsAdd" onSubmit={addPost}>
      <h3>Add new post</h3>
      <label htmlFor="postTitle" style={{display: 'block'}}>Title</label>
      <input id="postTitle" type="text" onChange={onTitleChange} value={title} required />
      <label htmlFor="postDescription" style={{display: 'block'}}>Description</label>
      <textarea id="postDescription" rows="10" onChange={onDescriptionChange} value={description} required />
      <button type="submit">Save</button>
    </form>
  );
};

export default PostAdd;
