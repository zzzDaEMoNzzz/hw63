import React from 'react';
import Moment from "react-moment";
import './Post.css';

const Post = props => {
  let postDescription = null;
  if (props.description) {
    postDescription = (<div className="Post-description" >{props.description}</div>);
  }

  let readMoreBtn = null;
  if (!props.hideReadMore) {
    readMoreBtn = (
      <button
        onClick={() => props.readMoreHandler(props.id)}
      >
        Read more >>
      </button>
    )
  }

  let additionalButtons = null;
  if (props.showAdditionalButtons) {
    additionalButtons = (
      <div className="additionalButtons">
        <button type="button" onClick={() => props.deletePost(props.id)}>Delete</button>
        <button type="button" onClick={() => props.editPost(props.id)}>Edit</button>
      </div>
    );
  }

  return (
    <div className="Post">
      <div className="Post-date">
        <span>Created on: </span>
        {props.date ? <Moment format="DD.MM.YYYY HH:mm">{props.date}</Moment> : null}
      </div>
      <p className="Post-title">{props.title}</p>
      {postDescription}
      {readMoreBtn}
      {additionalButtons}
    </div>
  );
};

export default Post;
