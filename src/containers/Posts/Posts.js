import React, {Fragment, Component} from 'react';
import {Route, Switch, withRouter} from "react-router-dom";
import axios from "axios";
import moment from "moment";

import Post from "./Post/Post";
import PostAdd from "./PostAdd/PostAdd";
import PostInfo from "./PostInfo/PostInfo";
import PostEdit from "./PostEdit/PostEdit";

class Posts extends Component {
  state = {
    postInfo: {},
    postAddTitle: '',
    postAddDescription: '',
  };

  postAddTitleChange = event => {
    this.setState({postAddTitle: event.target.value});
  };

  postAddDescriptionChange = event => {
    this.setState({postAddDescription: event.target.value});
  };

  addPost = event => {
    event.preventDefault();

    this.props.preloaderShow();

    axios.post('posts.json', {
      title: this.state.postAddTitle,
      description: this.state.postAddDescription,
      date: moment().valueOf()
    }).finally(() => {
      this.props.preloaderHide();
      this.setState({
        postAddTitle: '',
        postAddDescription: ''
      });
      this.props.history.push('/');
    });
  };

  readMoreHandler = id => {
    this.props.history.push('/posts/' + id);
  };

  getInfo = id => {
    this.props.preloaderShow();

    axios.get('posts/' + id + '.json').then(response => {
      this.setState({postInfo: {id, ...response.data}});
    }).finally(() => this.props.preloaderHide());
  };

  deletePost = id => {
    if (window.confirm('Are you want to delete this post?')) {
      this.props.preloaderShow();

      axios.delete('posts/' + id + '.json').finally(() => {
        this.props.history.replace('/');
      }).finally(() => this.props.preloaderHide());
    }
  };

  editPostBtnHandler = id => {
    this.props.history.push('/posts/' + id + '/edit');
  };

  editPost = (event, id, title, description, date) => {
    event.preventDefault();

    this.props.preloaderShow();

    axios.put('posts/' + id + '.json', {
      title,
      description,
      date
    }).finally(() => {
      this.props.preloaderHide();
      this.props.history.replace('/');
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const pathnameArray = this.props.location.pathname.split('/');

    if (pathnameArray[pathnameArray.length - 2] === 'posts') {
      const postId = pathnameArray[pathnameArray.length - 1];
      if (!this.state.postInfo.id || (this.state.postInfo.id && this.state.postInfo.id !== postId)) {
        this.getInfo(postId);
      }
    }

    const isPostsPage = this.props.location.pathname === '/' || this.props.location.pathname === '/posts/';
    const locationDifference = this.props.location.pathname !== prevProps.location.pathname;
    if (isPostsPage && locationDifference) {
      this.setState({postInfo: {}});
    }
  }

  render() {
    return (
      <Fragment>
        <Switch>
          <Route path="/" exact render={() => {
            return [...this.props.posts].reverse().map(post => {
              return (<Post key={post.id} id={post.id} readMoreHandler={this.readMoreHandler} title={post.title} date={post.date} />);
            });
          }}/>
          <Route path="/posts/" exact render={() => {
            return [...this.props.posts].reverse().map(post => {
              return (<Post key={post.id} id={post.id} readMoreHandler={this.readMoreHandler} title={post.title} date={post.date} />);
            });
          }}/>
          <Route path="/posts/add/" exact render={() => (
            <PostAdd
              title={this.state.postAddTitle}
              description={this.state.postAddDescription}
              onTitleChange={this.postAddTitleChange}
              onDescriptionChange={this.postAddDescriptionChange}
              addPost={this.addPost}
            />
          )}/>
          <Route path="/posts/:id/edit/" exact render={() => (
            <PostEdit
              data={this.state.postInfo}
              saveChanges={this.editPost}
            />
          )}/>
          <Route path="/posts/:id/" render={() => (
            <PostInfo
              data={this.state.postInfo}
              deletePost={this.deletePost}
              editPost={this.editPostBtnHandler}
            />
          )}/>
        </Switch>
      </Fragment>
    );
  }
}

export default withRouter(Posts);
