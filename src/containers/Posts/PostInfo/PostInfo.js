import React, {Fragment} from 'react';
import Post from "../Post/Post";

const PostInfo = ({data, deletePost, editPost}) => {
  return (
    <Fragment>
      <Post
        key={data.id}
        id={data.id}
        date={data.date}
        title={data.title}
        description={data.description}
        hideReadMore
        showAdditionalButtons
        deletePost={deletePost}
        editPost={editPost}
      />
    </Fragment>
  );
};

export default PostInfo;
