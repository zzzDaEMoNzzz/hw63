import React, {Component} from 'react';
import './PostEdit.css';

class PostEdit extends Component {
  state = {
    postEditTitle: '',
    postEditDescription: ''
  };

  componentDidMount() {
    this.setState({
      postEditTitle: this.props.data.title,
      postEditDescription: this.props.data.description
    })
  }

  postEditTitleChange = event => {
    this.setState({postEditTitle: event.target.value});
  };

  postEditDescriptionChange = event => {
    this.setState({postEditDescription: event.target.value});
  };

  render() {
    let content = (<div>Something Wrong</div>);
    if (this.props.data.id) {
      content = (
        <form
          className="PostEdit"
          onSubmit={event => this.props.saveChanges(event, this.props.data.id, this.state.postEditTitle, this.state.postEditDescription, this.props.data.date)}
        >
          <h3>Edit post</h3>
          <label htmlFor="postTitle" style={{display: 'block'}}>Title</label>
          <input id="postTitle" type="text" onChange={this.postEditTitleChange} value={this.state.postEditTitle} required />
          <label htmlFor="postDescription" style={{display: 'block'}}>Description</label>
          <textarea id="postDescription" rows="10" onChange={this.postEditDescriptionChange} value={this.state.postEditDescription} required />
          <button type="submit">Save</button>
        </form>
      );
    }

    return content;
  }
}

export default PostEdit;
