import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import axios from 'axios';
import {BrowserRouter} from "react-router-dom";
axios.defaults.baseURL = 'https://kurlov-hw63.firebaseio.com/';

const app = (
  <BrowserRouter>
    <App/>
  </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));