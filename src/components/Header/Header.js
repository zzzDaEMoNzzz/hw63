import React from 'react';
import './Header.css';
import {Link, NavLink} from "react-router-dom";

const Header = ({logo, links}) => {
  return (
    <header className="Header">
      <Link to="/" className="logo">{logo}</Link>
      <nav>
        {links.reduce((array, link) => {
          array.push(
            <NavLink
              to={link.url}
              exact={link.exact}
              isActive={link.isActive ? link.isActive : null}
              key={array.length}
            >
              {link.text}
            </NavLink>
          );

          return array;
        }, [])}
      </nav>
    </header>
  );
};

export default Header;
