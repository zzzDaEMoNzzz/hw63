import React, {Fragment} from 'react';

const Contacts = () => {
  return (
    <Fragment>
      <h3>Contacts</h3>
      <p><b>Address:</b> some address</p>
      <p><b>Phone:</b> some phone</p>
      <p><b>E-Mail:</b> some email</p>
    </Fragment>
  );
};

export default Contacts;
